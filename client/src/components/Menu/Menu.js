import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import List from '@mui/material/List';
import { listItems } from "./ListItems";
import { Link } from 'react-router-dom';

export default () => {
    return (
        <List>
            { listItems.map(({ icon, text, path }) => {
                return (
                    <Link style={{color: "inherit", textDecoration: "none"}} to={path}>
                        <ListItem button>
                            <ListItemIcon>
                                {icon}
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    </Link>
                )
            })}

        </List>
    )
}