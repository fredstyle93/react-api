import ListIcon from '@mui/icons-material/List';
import Code from '@mui/icons-material/Code';
import Home from '@mui/icons-material/Home';

export const listItems = [
    {
        icon: <Home />,
        text: "Home",
        path: "/home"
    },
    {
        icon: <ListIcon />,
        text: "Todo list",
        path: "/todo"
    },
    {
        icon: <Code />,
        text: "Counter",
        path: "/counter"
    },

]