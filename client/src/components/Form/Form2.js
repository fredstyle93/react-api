import { Button, Grid, TextField } from "@mui/material";
import React, { useContext, useState } from "react";
import { ToDoContext } from "../../context/TodoContext";

export default () => {
  const [todo, setTodo] = useState("");
  const {addToDo} = useContext(ToDoContext);
  const handleChangeToDo = ({ target }) => {
    const { value } = target;
    setTodo(value);
  }

  const handleToDoAdd = () => {
    setTodo('');
    addToDo(todo)
  }
  return (
    <Grid container>
      <Grid item md={11}>
        <TextField onChange={handleChangeToDo} value={todo} style={{width: "100%"}} id="standard-basic" label="todo" variant="standard" />
      </Grid>
      <Grid item md={1}>
        <Button onClick={handleToDoAdd} variant="contained">Add</Button>
      </Grid>
    </Grid>
  )
}