import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/icons-material/Delete';
import { ToDoContext } from '../../context/TodoContext';

export default () => {
  const {todos, removeToDo, toggleEditing} = React.useContext(ToDoContext);
  return (
    <List sx={{ width: '100%', maxWidth: "100%", bgcolor: 'background.paper' }}>
      {todos?.map(({name, id, isEditing}) => {
        const labelId = `checkbox-list-label-${name}`;

        return (
          <ListItem
            onClick={() => toggleEditing(id)}
            key={name}
            secondaryAction={
              <IconButton onClick={() => removeToDo(id)} edge="end" aria-label="comments">
                <Delete />
              </IconButton>
            }
          >
            <ListItemButton role={undefined} dense>
            {!isEditing ? (
              <ListItemText id={labelId} primary={name} />
            ) : (
              "yo"
            )}
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
  )}