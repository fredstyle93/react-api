import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/icons-material/Delete';

export default ({ list, handleRemoveToDo }) => {
  return (
    <List sx={{ width: '100%', maxWidth: "100%", bgcolor: 'background.paper' }}>
      {list?.map(({name, id}) => {
        const labelId = `checkbox-list-label-${name}`;

        return (
          <ListItem
            key={name}
            secondaryAction={
              <IconButton onClick={() => handleRemoveToDo(id)} edge="end" aria-label="comments">
                <Delete />
              </IconButton>
            }
          >
            <ListItemButton role={undefined} dense>
              <ListItemText id={labelId} primary={name} />
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
  )}