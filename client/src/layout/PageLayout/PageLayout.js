import React from "react";
import Form from "../../components/Form/Form";
import List from "../../components/List/List";
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';


const PageLayout = ({ children }) => {
    return (
        <Container maxWidth="lg" sx={{ mt: 10, mb: 4 }}>
        <Grid container spacing={3}>
          <Grid item md={12}>
            <Paper
              sx={{
                p: 2,
                display: 'flex',
                flexDirection: 'column',
              }}
            >
                {children}
            </Paper>
          </Grid>
          </Grid>
      </Container>
    )
}

export default PageLayout;