import * as React from 'react';
import { Route, Routes } from 'react-router-dom';

import Home from "./pages/home/Home";
import ToDo from "./pages/todo/ToDo";
import Layout from "./layout/Layout/Layout";
import Counter from './pages/counter/Counter';
import PageLayout from './layout/PageLayout/PageLayout';
import ToDo2 from './pages/todo/ToDo2';

export default () => {
  return (
    <Layout>
      {/* {RoutesObject.map((props) => <Route {...props} />)} */}
      <PageLayout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/counter" element={<Counter />} />
          <Route path="/todo" element={<ToDo2 />} />
        </Routes>
      </PageLayout>
    </Layout>
  )
}