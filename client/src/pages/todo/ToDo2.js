import { useState } from "react";
import Form from "../../components/Form/Form2";
import List from "../../components/List/List2";
import { ToDoProvider } from "../../context/TodoContext";

export default () => {
  const [todos, setTodos] = useState([]);

  
  const handleAddToDo = (todo) => {
    setTodos([...todos, {
      id:todos.length,
      name: todo,
      isDone: false,
      isEditing: false,
    }]);
  }

  const handleRemoveToDo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id));
  }


    return (
      <ToDoProvider>
        <Form />
        <List />
      </ToDoProvider>
    )
}