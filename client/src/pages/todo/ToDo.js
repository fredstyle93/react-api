import { useState } from "react";
import Form from "../../components/Form/Form";
import List from "../../components/List/List";

export default () => {
  const [todos, setTodos] = useState([]);

  
  const handleAddToDo = (todo) => {
    setTodos([...todos, {
      id:todos.length,
      name: todo,
      isDone: false,
    }]);
  }

  const handleRemoveToDo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id))
  }


    return (
      <>
        <Form toDoAddCallBack={handleAddToDo} />
        <List handleRemoveToDo={handleRemoveToDo} list={todos} />
      </>
    )
}