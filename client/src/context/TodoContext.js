import React, { createContext, useEffect, useState } from 'react';

export const ToDoContext = createContext({
    todos: []
});

export const ToDoProvider = ({ children }) => {
const [todos, setTodos] = useState([]);

    const addToDo = (todo) => {
        setTodos([...todos, {
            id:todos.length,
            name: todo,
            isDone: false,
            isEditing: false,
          }]);
    }

    const removeToDo = (id) => {
        setTodos(todos.filter(todo => todo.id !== id));
    }

    const toggleEditing = (id) => {
        const newTodo = todos.map((todo) => {
            if(todo.id === id) {
                return {
                    ...todo,
                    isEditing: !todo.isEditing
                }
            }
            return todo;
        })
        setTodos(newTodo)
    }

  const contextValue = React.useMemo(
    () => ({
        todos,
        addToDo,
        removeToDo,
        toggleEditing
    }),
    [todos]
  );

  return (
    <ToDoContext.Provider value={contextValue}>{children}</ToDoContext.Provider>
  );
};