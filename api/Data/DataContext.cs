﻿using api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<ToDoModel> ToDos { get; set; }

        protected override void OnModelCreating(ModelBuilder model)
        {
            model.Entity<ToDoModel>().HasData(new ToDoModel { Id = 1, IsEditing = false, IsCompleted = false , Name = "Test"});
        }

    }
}
