﻿using api.Models;
using api.Services.ToDo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Controllers
{
    [ApiController]

    [Route("api")]
    public class ToDoController: Controller
    {
        private readonly IToDoService _toDoService;

        public ToDoController(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }
        [HttpGet("todos")]
        public async Task<ActionResult<List<ToDoModel>>> Get()
        {
            var resp = await _toDoService.GetToDos();
            return Ok(resp);
        }

        [HttpPost("todos")]
        public async Task<ActionResult<List<ToDoModel>>> Post(ToDoModel todo)
        {
            var resp = await _toDoService.CreateToDo(todo);
            return Ok(resp);
        }
    }
}
