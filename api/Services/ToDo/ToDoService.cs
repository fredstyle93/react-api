﻿using api.Data;
using api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Services.ToDo
{
    public class ToDoService : IToDoService
    {
        private readonly DataContext _dataContext;
        public ToDoService (DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<ToDoModel>> CreateToDo(ToDoModel todo)
        {
            await _dataContext.ToDos.AddAsync(todo);
            await _dataContext.SaveChangesAsync();


            return await _dataContext.ToDos.ToListAsync<ToDoModel>();
        }

        public async Task<List<ToDoModel>> GetToDos()
        {
            var todos = await _dataContext.ToDos.ToListAsync();

            return todos;
        }
    }
}
