﻿using api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Services.ToDo
{
    public interface IToDoService
    {
        public Task<List<ToDoModel>> GetToDos();
        public Task<List<ToDoModel>> CreateToDo(ToDoModel todo);
    }
}
